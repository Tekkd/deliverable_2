﻿using StatisticsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc; 

namespace StatisticsAPI.ViewModel
{
    public class AdvancedVM
    {
        //Fields For Report Criteria 
        public IEnumerable<SelectListItem> Malaria { get; set; }
        public int SelectedMalariaID { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

        //Fields For Report data 
        public Malaria malariaR { get; set; }
        public List<IGrouping<string,ReportRecord>> results { get; set; }
        public Dictionary<string, double> ChartData { get; set; }

        public class ReportRecord
        {
            public string Patient_FName { get; set; }
            public string Patient_LName { get; set; }
            public string Diagnosis_Date { get; set; }
            public string Malaria_Name { get; set; }
            public string Malaria_Description { get; set; }
            public string Treatment_Name { get; set; }
            public double MalariaID { get; set; }
        }
    }
}