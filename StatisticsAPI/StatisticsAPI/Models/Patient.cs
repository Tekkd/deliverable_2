//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StatisticsAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Patient
    {
        public int PatientID { get; set; }
        public string Patient_FName { get; set; }
        public string Patient_LName { get; set; }
        public string Patient_Cellphone { get; set; }
        public string Patient_Email { get; set; }
        public Nullable<System.DateTime> Diagnosis_Date { get; set; }
        public int MalariaID { get; set; }
    
        public virtual Malaria Malaria { get; set; }
    }
}
